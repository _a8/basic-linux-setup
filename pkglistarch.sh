﻿#!/bin/bash -l
#############################################################
#############################################################
##                  basic-linux-setup                      ##
#############################################################
##             https://github.com/thanasxda                ##
#############################################################
##      15927885+thanasxda@users.noreply.github.com        ##
#############################################################
##    https://github.com/thanasxda/basic-linux-setup.git   ##
#############################################################
#############################################################

#{

yellow="\033[1;93m"
restore="\033[0m"
### variables
source="$(pwd)"
tmp=$source/tmp
s="sudo"
echo -e "${yellow}"
echo "" && echo "" && echo "" && echo "pkglist starting..." && echo "" && echo "" && echo ""




# thanas

$s pacman -S --noconfirm util-linux-libs gptfdisk iputils net-tools nftables perl wireless_tools iw busybox dracut systemd-ui systemd-sysvcompat f2fs-tools xfsprogs attr linux-firmware kmod plasma-desktop sddm cronie dracut bc neofetch kmodlinux-xanmod-bin

$s pacman -S --noconfirm kdeconnect \
 discover \
 flatpak \
 fwupd \
 partitionmanager \
 gstreamer phonon-qt5-gstreamer gstreamer-vaapi qt-gstreamer \
 python-pip \
 links \
 openssh \
 putty \
 rng-tools \
 rt-tests \
 shellcheck \
 uget \
 ccache \
 gedit \
 vlc \
 hdparm \
 ethtool \
 xsettingsd \
 virt-manager \
 nmap \
 apparmor \
 diffuse \
 haveged \
 jitterentropy \
 htop \
 psensor \
 wireless-regdb \
 binwalk \
 mc \
 rtirq \
 lz4 \
 kvantum-theme-materia kvantum \
 kio-fuse kio-extras kio \
 firewalld \
 kwrite \
 dolphin dolphin-plugins \
 irqbalance \
 openssl \
 efibootmgr \
 dbus-broker \
 xorg-xinit \
 fail2ban \
 android-tools android-udev \
 dpkg \
 lz4 \
 macchanger \
 kdebugsettings



yay -S --noconfirm muon \
alsa-tools \
kernel-install-for-dracut systemd-boot-pacman-hook dracut-hook-uefi dkms \
debtap 


yay -S --noconfirm preload

pip install requests

$s flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

if
glxinfo | grep -q Intel ; then
$s pacman -S --noconfirm vulkan-intel libva-intel-driver ; elif
glxinfo | grep -q NVIDIA ; then
$s pacman -S --noconfirm nvidia-utils libvdpau ; elif
glxinfo | grep -q AMD ; then
$s pacman -S --noconfirm vulkan-radeon amdvlk
fi

pacman -S --noconfirm ffmpeg ffmpeg4.4 x264 x265 aom dav1d svt-av1 schroedinger libdv libmpeg2 libtheora libvpx

pacman -S --noconfirm libva-mesa-driver mesa-utils mesa-vdpau opencl-mesa vulkan-mesa-layers libva-mesa-driver libva-utils libva-vdpau-driver libvdpau-va-gl vdpauinfo vulkan-icd-loader

if lscpu | grep -q Intel ; then
yay -S --noconfirm iucode-tool intel-ucode-clear
fi

yay -S --noconfirm linux-zen linux-clear-bin linux-xanmod-bin 

yay -S --noconfirm dkms

#####################################################################################################
####### END #########################################################################################
#####################################################################################################
