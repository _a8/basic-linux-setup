[Appearance]
ColorScheme=Breeze
DimmValue=34
EmojiFont=Noto Color Emoji,14,-1,5,50,0,0,0,0,0
Font=Hack Nerd Font,9,-1,5,50,0,0,0,0,0
TabColor=42,99,255
UseFontLineChararacters=true
WordMode=false

[Cursor Options]
CursorShape=0
CustomCursorColor=212,164,21
CustomCursorTextColor=109,11,255
UseCustomCursorColor=true

[General]
DimWhenInactive=false
InvertSelectionColors=false
Name=basic
Parent=FALLBACK/
SemanticInputClick=true
SemanticUpDown=true
ShowTerminalSizeHint=false
TerminalCenter=true

[Interaction Options]
AllowEscapedLinks=false
CtrlRequiredForDrag=false
DropUrlsAsText=true
OpenLinksByDirectClickEnabled=false
TextEditorCmd=1
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
ReflowLines=true

[Terminal Features]
BlinkingCursorEnabled=false
UrlHintsModifiers=0
VerticalLine=false
